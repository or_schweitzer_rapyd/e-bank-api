import log from '@ajar/marker';
import app from './config/app.js';

const { PORT, HOST } = process.env;

(() => {
  try {
    app.listen(Number(PORT), HOST as string);
    log.magenta(`api is live on ✨ ⚡  http://${HOST as string}:${Number(PORT)} ✨⚡`);
  } catch (err) {
    log.red(err);
  }
})();