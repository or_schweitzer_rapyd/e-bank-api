import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import common_router from '../routes/common.router.js';
import individual_router from '../routes/individual.router.js';
import account_router from '../routes/account.router.js';
import business_router from '../routes/business.router.js';
import family_router from '../routes/family.router.js';

import {
  errorResponse,
  logErrorMiddleware,
  printError,
} from '../middlewares/errors.middlewares.js';
import { httpLogger } from '../middlewares/common.middlewares.js';
import fs from 'fs';
import { InputValidationService } from '../modules/validation/validation.service.js';
import { INPUT_VALIDATION_FUNCTIONS } from '../utils/validation.utils.js';

class App {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.configEnvVariables();
    this.configValidation();
    this.configExpressMiddlewares();
    this.configCustomMiddlewares();
    this.configRoutes();
    this.configErrorHandlers();
  }

  configValidation() {
    InputValidationService.configureValidator(INPUT_VALIDATION_FUNCTIONS);
  }

  configEnvVariables() {
    process.env = JSON.parse(fs.readFileSync('./constants.json', 'utf8'));
  }

  configErrorHandlers() {
    const { ERR_PATH = './logs/PLACEHOLDER.errors.log' } = process.env;
    this.app.use(printError);
    this.app.use(logErrorMiddleware(ERR_PATH));
    this.app.use(errorResponse);
  }

  private configExpressMiddlewares() {
    this.app.use(cors());
    this.app.use(morgan('dev'));
    this.app.use(express.json());
  }

  private configRoutes() {
    // Other routes here
    this.app.use('/api/family', family_router);
    this.app.use('/api/business', business_router);
    this.app.use('/api/individual', individual_router);
    this.app.use('/api/account', account_router);
    this.app.use('*', common_router);
  }

  private configCustomMiddlewares() {
    const { LOG_PATH = './logs/PLACEHOLDER.http.log.txt' } = process.env;
    this.app.use(httpLogger(LOG_PATH));
  }
}

export default new App().app;
