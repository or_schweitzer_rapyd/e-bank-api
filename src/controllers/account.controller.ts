/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { RequestHandler } from 'express';
//import { IAccount } from '../models/account.model.js';
import account_service from '../services/account.service.js';
import transfer_service from '../services/transfer.service.js';

class AccountController {
  setStatus: RequestHandler = async (req, res) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access

    await account_service.setStatus(req.body.ids, req.body.status);

    res.status(200).json({ response: 'some response' });
  };

  createTransferB2B: RequestHandler = async (req, res) => {
    const response = await transfer_service.createTransactionB2B(req.body);

    res.status(200).json({ response });
  };

  createTransferB2I: RequestHandler = async (req, res) => {
    await transfer_service.createTransactionB2I(req.body);
    res.status(200).json({ response: 'some response' });
  };

  createTransferF2B: RequestHandler = async (req, res) => {
    await transfer_service.createTransactionF2B(req.body);
    res.status(200).json({ response: 'some response' });
  };
}

export default new AccountController();
