import { RequestHandler } from 'express';
import { IBusinessAccount } from '../models/account.model.js';
import business_service from '../services/business.service.js';

class BusinessController {
  createBusiness: RequestHandler = async (req, res) => {
    const business = await business_service.create(req.body as IBusinessAccount);

    res.status(200).json(business);
  };

  getBusiness: RequestHandler = async (req, res) => {
    const business_id = Number(req.params.id);
    const business = await business_service.findById(business_id);

    res.status(200).json(business);
  };

  getAllBusinesses: RequestHandler = async (req, res) => {
    const stam = await 5;

    res.status(200).json({ stam });
  };
}

export default new BusinessController();
