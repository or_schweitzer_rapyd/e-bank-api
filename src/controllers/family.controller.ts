import { RequestHandler } from 'express';
import * as converter from '../converters/family.converter.js';
import { IFamilyAccount } from '../models/account.model.js';
import family_service from "../services/family.service.js";

class FamilyController {
    createFamily: RequestHandler = async (req, res) => {
      const stam = await 5;
      const family_db = converter.familyModelToDb(req.body as IFamilyAccount);
      console.log(stam);
      res.status(200).json(family_db);
    };
    
    getFamily: RequestHandler = async (req, res) => {
      await family_service.findById(Number(req.params.id))
      res.status(200).json({ status: 'final project' });
    };

    closeFamily: RequestHandler = async (req, res) => {
      await family_service.findById(Number(req.params.id))
      res.status(200).json({ status: 'final project' });
    };

    getAllFamillies: RequestHandler = async (req, res) => {
      await new Promise(() => console.log("get all famillies"));
      
      res.status(200).json({ status: 'final project' });
    };

    removeMembers: RequestHandler = async (req, res) => {
      await new Promise(() => console.log("remove members"));
    
    res.status(200).json({ status: 'final project' });
  };
  
  addMembers: RequestHandler = async (req, res) => {
    await new Promise(() => console.log("remove members"));
  
  res.status(200).json({ status: 'final project' });
  };


 
}

export default new FamilyController();
