import { RequestHandler } from 'express';
import { IIndividualAccount } from '../models/account.model.js';
// import { ITransactionDB } from '../models/transaction.db.model.js';
// import transactionRepository from '../repositories/transaction.repository.js';
import individual_service from '../services/individual.service.js';

class IndividualController {
  createIndividual: RequestHandler = async (req, res) => {
    // const transactions=await transactionRepository.transaction(req.body as ITransactionDB,100,200);
    // res.status(200).json(transactions);

    const individual = await individual_service.create(req.body as IIndividualAccount);
    res.status(200).json(individual);
  };

  getIndividual: RequestHandler = async (req, res) => {
    const individual_id = Number(req.params.id);
    const individual = await individual_service.findById(individual_id);

    res.status(200).json(individual);
  };

  getAllIndividuals: RequestHandler = async (req, res) => {
    // const individuals= await individualRepository.getAllIndividualDevDB();
    res.status(200).json({});
  };
}

export default new IndividualController();
