import { RowDataPacket } from 'mysql2';
import { IAccountDB } from '../models/account.db.model.js';
import { IAccount } from '../models/account.model.js';

class AccountConverter {
  rowDataPacketToModel(db_result: RowDataPacket[]) {
    console.log('db result is');
    console.log(db_result);
    const rowData = (db_result[0] || db_result) as any;
    console.log('account rowData is----------------------');
    console.log(rowData);

    console.log('hello from rowData');
    const { account_id, currency, balance, status, type } = rowData;

    const account: IAccount = {
      account_id,
      currency,
      balance,
      status,
      type,
    };

    return account;
  }

  rowDataPacketToModels(db_result: RowDataPacket[]) {
    const account_models = db_result.map(row_data => this.rowDataPacketToModel([row_data]));
    console.log('returning accounts');
    return account_models;
  }

  accountModelToDb(account_model: IAccount): IAccountDB {
    const { currency, balance, status, type } = account_model;

    const account_db: IAccountDB = {
      account_id: null,
      agent_id: -1,
      currency,
      type,
      balance,
      status,
    };

    return account_db;
  }
}

const account_converter = new AccountConverter();
export default account_converter;
