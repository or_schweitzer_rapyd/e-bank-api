import { RowDataPacket } from 'mysql2';
import { IAddressDB } from '../models/account.db.model.js';
import { IAddress } from '../models/account.model.js';

class AddressConverter {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  rowDataPacketToModel(db_result: RowDataPacket[]): IAddress {
    const rowData = (db_result[0] || db_result) as any;
    const {
      address_id = -1,
      country_name,
      country_code,
      postal_code,
      city,
      region,
      street_name,
      street_number,
    } = rowData;

    const address: IAddressDB = {
      address_id,
      country_name,
      country_code,
      postal_code,
      city,
      region,
      street_name,
      street_number,
    };

    return address;
  }

  rowDataPacketToModels(db_result: RowDataPacket[]) {
    db_result.map(row_data => this.rowDataPacketToModel([row_data]));
  }
}

const address_converter = new AddressConverter();

export default address_converter;
