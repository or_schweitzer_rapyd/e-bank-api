/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { RowDataPacket } from 'mysql2';
import { IAccount, IBusinessAccount } from '../models/account.model.js';
import { IAccountDB, IAddressDB, IBusinessAccountDB } from '../models/account.db.model.js';
import account_converter from './account.converter.js';
import address_converter from './address.converter.js';

class BusinessConverter {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  rowDataPacketToModel(db_result: RowDataPacket[]): IBusinessAccount {
    const rowData = db_result[0] as any;
    const { company_id, company_name, account_info_id, context } = rowData;
    type IAccountNoTypeAndId = Partial<IAccount>;

    const account: IAccountNoTypeAndId = account_converter.rowDataPacketToModel(rowData);

    console.log('account is');
    console.log(account);
    console.log('address is');
    const address = address_converter.rowDataPacketToModel(rowData);
    console.log(address);

    delete account.account_id;

    delete account.type;

    const business: IBusinessAccount = {
      account_id: account_info_id,
      ...(account as IAccount),
      address,
      company_id,
      company_name,
      context,
    };

    return business;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  businessModelToDb(
    business_account: IBusinessAccount,
  ): [IAccountDB, IAddressDB, IBusinessAccountDB] {
    const { address, currency, balance, status, company_id, company_name, context } =
      business_account;

    //parse account data
    const account_db: IAccountDB = {
      account_id: null,
      agent_id: 0,
      currency,
      balance,
      status,
      type: 'Business',
    };

    //parse address data
    const address_db: IAddressDB = { ...address };

    //parse business data
    const business_db: IBusinessAccountDB = {
      company_id,
      company_name,
      context,
      address_id: -1,
    };

    return [account_db, address_db, business_db];
  }
}

const business_converter = new BusinessConverter();
export default business_converter;
