/* eslint-disable @typescript-eslint/no-unused-vars */
import { RowDataPacket } from 'mysql2';
import { IIndividualAccount } from '../models/account.model.js';
import { IAccountDB, IAddressDB, IIndividualAccountDB } from '../models/account.db.model.js';
import { Console } from 'console';

// eslint-disable-next-line @typescript-eslint/no-unused-vars

class IndividualConverter {
  rowDataPacketToModel(db_result: RowDataPacket[]): IIndividualAccount {
    const rowData = db_result[0] as any;
    console.log('rowData is----------------------');
    console.log(rowData);

    console.log('hello from rowData');
    const {
      individual_id,
      account_info_id,
      currency,
      balance,
      status,
      person_id,
      first_name,
      last_name,
      email,
      country_name,
      country_code,
      postal_code,
      city,
      region,
      street_name,
      street_number,
    } = rowData;

    const individual: IIndividualAccount = {
      // account_id: individual_id,
      account_id: account_info_id,
      currency,
      balance,
      status,
      individual_id: person_id,
      first_name,
      last_name,
      email,
      address: {
        country_name,
        country_code,
        postal_code,
        city,
        region,
        street_name,
        street_number,
      },

      type: 'Individual',
    };

    return individual;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  individualModelToDb(
    individual_account: IIndividualAccount,
  ): [IAccountDB, IAddressDB, IIndividualAccountDB] {
    const {
      individual_id,
      first_name,
      last_name,
      email,
      address,
      account_id = -1,
      currency,
      type,
      balance,
      status,
    } = individual_account;

    //parse account data
    const account_db: IAccountDB = {
      account_id: null,
      agent_id: 0,
      currency,
      balance,
      status,
      type: 'Individual',
    };

    //parse address data
    const { address_id = -1 } = address;

    const address_db: IAddressDB = { ...address, address_id };

    //parse individual data
    const individual_db: IIndividualAccountDB = {
      individual_id: account_id,
      person_id: individual_id,
      first_name,
      last_name,
      email,
      address_id,
      account_info_id: -1,
    };

    return [account_db, address_db, individual_db];
  }
}

const individual_converter = new IndividualConverter();

export default individual_converter;
