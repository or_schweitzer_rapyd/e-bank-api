export class HttpException extends Error {
  constructor(public status: number, public message: string) {
    super(message);
  }
}

export class TransactionException extends HttpException {
  constructor(public status: number, public message: string) {
    super(400, 'Transaction failed');
  }
}
