import { IAccount, IBusinessAccount, IFamilyAccount } from '../models/account.model';

// Individual
export const validateIndividualCreation = (account: IAccount): boolean => {
    return doesAccountExist(account);
};

// Family

export const validateFamilyCreation = (accounts: IAccount[], accounts_tuples: [number, number][], expected_currency: string): boolean => {
    return (
        doAccountsExist(accounts, accounts_tuples.length) &&
        checkAccountsStatuses(accounts, 'active') &&
        checkAccountsTypes(accounts, 'individual') &&
        checkAccountsCurrencies(accounts, expected_currency) &&
        checkIfIndividualsHaveEnoughMoney(accounts, accounts_tuples.map((tuple:[number,number]) => tuple[1]))
    );
}


export const validateAddMembersToFamily = (family_account: IFamilyAccount, accounts_to_add: IAccount[], accounts_tuples: [number, number][]):boolean => {
    return (
        doesAccountExist(family_account) &&
        checkAccountType(family_account,'family') &&
        checkAccountStatus(family_account,'active') &&
        doAccountsExist(accounts_to_add, accounts_tuples.length) &&
        checkAccountsStatuses(accounts_to_add, 'active') &&
        checkAccountsTypes(accounts_to_add, 'individual') &&
        checkAccountsCurrencies(accounts_to_add, family_account.currency)
    );

    // same currency family and accounts_to_add
    // accounts to add Have balance minimum 1000 after transaction
}

// Transactions
export const validateTransferB2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
        doesAccountExist(src) &&
        doesAccountExist(dest) &&
        checkAccountStatus(src, 'active') &&
        checkAccountStatus(dest, 'active') &&
        checkAccountType(src, 'business') &&
        checkAccountType(dest, 'business') &&
        checkAccountCurrency(dest, src.currency) &&
        checkTransferLogic('B2B', src, dest, amount)
    );
};

export const validateTransferB2BFX = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
        doesAccountExist(src) &&
        doesAccountExist(dest) &&
        checkAccountStatus(src, 'active') &&
        checkAccountStatus(dest, 'active') &&
        checkAccountType(src, 'business') &&
        checkAccountType(dest, 'business') &&
        !checkAccountCurrency(dest, src.currency) &&
        checkTransferLogic('B2B', src, dest, amount)
    );
}

export const validateTransferB2I = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
        doesAccountExist(src) &&
        doesAccountExist(dest) &&
        checkAccountStatus(src, 'active') &&
        checkAccountStatus(dest, 'active') &&
        checkAccountType(src, 'business') &&
        checkAccountType(dest, 'individual') &&
        checkAccountCurrency(dest, src.currency) &&
        checkTransferLogic('B2I', src, dest, amount)
    );
}

export const validateTransferF2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
        doesAccountExist(src) &&
        doesAccountExist(dest) &&
        checkAccountStatus(src, 'active') &&
        checkAccountStatus(dest, 'active') &&
        checkAccountType(src, 'family') &&
        checkAccountType(dest, 'business') &&
        checkAccountsStatuses((src as IFamilyAccount).owners, 'active') &&
        checkAccountsTypes((src as IFamilyAccount).owners, 'individual') &&
        checkAccountCurrency(dest, src.currency) &&
        checkAccountsCurrencies((src as IFamilyAccount).owners, src.currency) &&
        checkTransferLogic("F2B", src, dest, amount)
    );
}





// utils
export const checkTransferLogic = (
    transaction_type: string,
    src: IAccount,
    dest: IAccount,
    amount: number,
): boolean => {
    switch (transaction_type) {
        case 'B2B': {
            let allowed_amount_to_transfer =
                (src as IBusinessAccount).company_id === (dest as IBusinessAccount).company_id
                    ? 10000
                    : 1000;
            return src.balance - amount >= 10000 && amount <= allowed_amount_to_transfer;
        }
        case 'B2I': {
            return src.balance - amount >= 10000 && amount <= 1000;
        }
        case 'F2B': {
            return src.balance - amount >= 5000 && amount <= 5000;
        }
    }
    return true;
};

export const doesAccountExist = (account: IAccount): boolean => {
    return !!account;
};

export const doAccountsExist = (accounts: IAccount[], expected_accounts_num: number): boolean => {
    return accounts.length === expected_accounts_num;
};

export const checkAccountsStatuses = (accounts: IAccount[], expected_status: string): boolean => {
    return accounts.every(account => checkAccountCurrency(account, expected_status));
};

export const checkAccountStatus = (account: IAccount, expected_status: string): boolean => {
    return account.status === expected_status;
};

export const checkAccountsTypes = (accounts: IAccount[], expected_type: string): boolean => {
    return accounts.every(account => checkAccountType(account, expected_type));
};

export const checkAccountType = (account: IAccount, expected_type: string): boolean => {
    return account.type === expected_type;
};

export const checkAccountsCurrencies = (accounts: IAccount[], expected_currency: string): boolean => {
    return accounts.every(account => checkAccountCurrency(account, expected_currency));
};

export const checkAccountCurrency = (account: IAccount, expected_currency: string): boolean => {
    return account.currency === expected_currency;
};

export const checkIfIndividualsHaveEnoughMoney = (accounts: IAccount[], amounts_to_transfer:number[]): boolean => {
    return accounts.every((account,index) => account.balance - amounts_to_transfer[index]);
};






// Family





// export const deleteAccountsFromFamily = (family: IFamilyAccount, accounts_to_delete: IAccount[], owner_tuples: any):boolean => {
//     // Exist family
//     // Exist accounts
//     // Accounts belong to family
//     // Family account minimum balance after transfer to individual is 5000 unless we're removing
//     // everyone and then the minimum is 0
// }

// export const closeFamilyAccount = (family: IFamilyAccount):boolean => {
//     // Exist family
//     // Check active
//     // No family members
// }

// export const activeDeactiveAccount = (account: IAccount[], action):boolean => {
//     // Exists
//     // If action is active then check all inactive
//     // If action is deactive then check all active
//     // All accounts are not family accounts
// }
