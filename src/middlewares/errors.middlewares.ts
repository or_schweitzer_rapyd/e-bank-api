import log from '@ajar/marker';
import { ErrorRequestHandler, RequestHandler } from 'express';
import fs from 'fs';
import { HttpException } from '../exceptions/http.exception.js';

const { NODE_ENV } = process.env;

export const printError: ErrorRequestHandler = (err: HttpException, req, res, next) => {
  log.error(err);
  next(err);
};

export function logErrorMiddleware(path: string): ErrorRequestHandler {
  const errorsFileLogger = fs.createWriteStream(path, { flags: 'a' });
  return (error: HttpException, req, res, next) => {
    errorsFileLogger.write(`${error.status} :: ${error.message} >> ${error.stack as string} \n`);
    next(error);
  };
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorResponse: ErrorRequestHandler = (err: HttpException, req, res, next) => {
  if (NODE_ENV !== 'production')
    res.status(err.status).json({
      status: err.status,
      message: err.message,
      stack: err.stack,
    });
  else
    res.status(err.status).json({
      status: err.status,
      message: err.message,
    });
};

export const urlNotFound: RequestHandler = (req, res, next) => {
  next(new HttpException(404, 'URL: ' + req.baseUrl + 'not found!'));
};
