import { NextFunction, Request, RequestHandler, Response } from 'express';
import { validation_config_object } from '../config/validation.config.js';
import { IGeneralObject, IValidationItem } from '../modules/validation/validation.model.js';
import { InputValidationService } from '../modules/validation/validation.service.js';
import { curry } from '../utils/common.utils.js';

export const inputValidationMiddleware = curry((entity: string, route: string): RequestHandler => {
  return (req: Request, res: Response, next: NextFunction): void => {
    const validation_result = InputValidationService.getValidationResult(
      (validation_config_object[entity] as IGeneralObject)[route] as IValidationItem[],
      req.body as IGeneralObject,
    );
    validation_result.length ? res.status(500).send({ messages: validation_result }) : next();
  };
});
