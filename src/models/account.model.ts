export interface IAccount {
  account_id?: number;
  currency: string;
  balance: number;
  status: 'active' | 'inactive';
  type: string;
}

export interface IIndividualAccount extends IAccount {
  individual_id: number;
  first_name: string;
  last_name: string;
  address: IAddress;
  email: string;
}

export interface IBusinessAccount extends IAccount {
  company_id: number;
  company_name: string;
  context: string;
  address: IAddress;
}

export interface IFamilyAccount extends IAccount {
  context: string;
  owners: IIndividualAccount[];
}

export interface IAddress {
  address_id?: number;
  country_name: string;
  country_code: string;
  postal_code: number;
  city: string;
  region: string;
  street_name: string;
  street_number: number;
}
