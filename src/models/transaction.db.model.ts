export interface ITransactionDB {
  transaction_id?: number;
  src_id: number;
  dest_id: number;
  transaction_type: string;
  amount: number;
}
