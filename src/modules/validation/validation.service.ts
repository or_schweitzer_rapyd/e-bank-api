import { IErrorItem, IRule, IValidationItem, IValidationFunctions, IGeneralObject } from './validation.model.js';


export class InputValidationService {
  private static valdiation_functions: IValidationFunctions = {
  };

  public static configureValidator(extended_functions: IValidationFunctions): void {
    InputValidationService.valdiation_functions = {
      ...InputValidationService.valdiation_functions,
      ...extended_functions
    };
  }

  private static getErrorList = (item: IValidationItem, input: any): IErrorItem[] => {
    const errorItems: IErrorItem[] = [];
    item.rules.forEach((rule: IRule) => {
      const operationType = Object.keys(rule)[0];
      const operationLimit = rule[operationType];
      const operatorFunc = InputValidationService.valdiation_functions[operationType];
      const passes = operatorFunc(input, operationLimit);
      if (passes) return true;
      else
        errorItems.push({
          content: rule.validation_message,
          info: 'Validation Error',
        });
    });
    return errorItems;
  };

  public static getValidationResult = (validation_items: IValidationItem[], validation_object: IGeneralObject): IErrorItem[] =>
    validation_items.reduce((acc: IErrorItem[], item: IValidationItem): IErrorItem[] => {
      const field: any = validation_object[item.key];
      const errItems = InputValidationService.getErrorList(item, field);
      const inputPassesValidation = field && item.rules && errItems.length < 1;
      return !inputPassesValidation ? [...acc, ...errItems] : acc;
    }, []);
}




export default new InputValidationService();
