import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { ITransactionDB } from '../models/transaction.db.model.js';
import accountRepository from './account.repository.js';

class TransactionRepository {
  private async recordTransactionDB(transaction: ITransactionDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO transaction SET ?';
    try {
      const rows = await connection.query(sql, transaction);
      const inserted_transaction_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return await this.getTransactionsByIdDB([inserted_transaction_id]);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  private async createTransaction(
    src_id: number,
    dest_id: number,
    src_balance: number,
    dest_balance: number,
  ): Promise<RowDataPacket[]> {
    const sql =
      'UPDATE account_information SET balance = ' +
      'CASE WHEN account_id = ? THEN ? WHEN account_id = ? THEN ? ' +
      'END';
    // ('WHERE account_id = ? OR account_id = ?');
    try {
      console.log(sql);

      await connection.query(sql, [src_id, src_balance, dest_id, dest_balance, src_id, dest_id]);

      const rows = await accountRepository.getAccountsByIdDB([src_id, dest_id]);
      return rows;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async transaction(
    transaction: ITransactionDB,
    src_balance: number,
    dest_balance: number,
  ): Promise<RowDataPacket[]> {
    await connection.beginTransaction();
    try {
      await this.createTransaction(
        transaction.src_id,
        transaction.dest_id,
        src_balance,
        dest_balance,
      );
      const accounts_transaction_info = await this.recordTransactionDB(transaction);
      await connection.commit();
      return accounts_transaction_info;
    } catch (err) {
      console.log(err);
      await connection.rollback();
      throw err;
    }
  }

  async getTransactionsByIdDB(transactions_id: number[]): Promise<RowDataPacket[]> {
    const sql = `SELECT * FROM transaction WHERE transaction_id IN (${
      '?' + ',?'.repeat(transactions_id.length - 1)
    })`;
    try {
      const [rows] = await connection.query(sql, transactions_id);
      return rows as RowDataPacket[];
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
export default new TransactionRepository();
