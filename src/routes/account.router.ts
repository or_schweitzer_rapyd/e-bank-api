import express from 'express';
import account_controller from '../controllers/account.controller.js';

import raw from '../middlewares/common.middlewares.js';

const account_router = express.Router();

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
account_router.put('/status', raw(account_controller.setStatus));

account_router.post('/b2b', raw(account_controller.createTransferB2B));

//account_router.post('/b2i',raw(account_controller.createTransferB2I));

//account_router.post('/f2b',raw(account_controller.createTransferF2B));

export default account_router;
