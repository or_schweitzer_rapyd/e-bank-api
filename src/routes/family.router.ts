import express from 'express';
import family_controller from '../controllers/family.controller.js';
import raw from '../middlewares/common.middlewares.js';

const family_router = express.Router();

family_router.post('/', raw(family_controller.createFamily));

family_router.get('/:id', raw(family_controller.getFamily));

family_router.delete('/:id', raw(family_controller.closeFamily));

family_router.get('/', raw(family_controller.getAllFamillies));

// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
family_router.delete('/add_members', raw(family_controller.addMembers));

family_router.delete('/remove_members', raw(family_controller.removeMembers));




export default family_router;
