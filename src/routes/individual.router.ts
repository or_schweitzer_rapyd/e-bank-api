import express from 'express';
import individual_controller from '../controllers/individual.controller.js';
import raw from '../middlewares/common.middlewares.js';

const individual_router = express.Router();

individual_router.post('/', raw(individual_controller.createIndividual));

individual_router.get('/:id', raw(individual_controller.getIndividual));

individual_router.get('/', raw(individual_controller.getAllIndividuals));

export default individual_router;
