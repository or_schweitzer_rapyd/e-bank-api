/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import account_repository from '../repositories/account.repository.js';
import account_converter from '../converters/account.converter.js';
import { IAccount } from '../models/account.model.js';

class AccountService {
  async setStatus(primary_ids: number[], status: string) {
    await account_repository.updateStatusesAccountsByIdDB(primary_ids, status);

    return [primary_ids, status];
  }

  async getTwoAccounts(account_ids: number[]): Promise<IAccount[]> {
    console.log(account_ids);

    const src_result_db = await account_repository.getAccountsByIdDB([account_ids[0]]);
    const dest_result_db = await account_repository.getAccountsByIdDB([account_ids[1]]);

    const src_model = account_converter.rowDataPacketToModel(src_result_db);
    const dest_model = account_converter.rowDataPacketToModel(dest_result_db);

    const account_models = [src_model, dest_model];

    return account_models;
  }
}

const account_service = new AccountService();
export default account_service;
