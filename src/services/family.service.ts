
import { IFamilyAccount } from "../models/account.model.js";
import { ICreateFamilyAccountDTO } from "../models/dto.model.js";
import family_repository  from "../repositories/family.repository.js";


class FamilyService{

    async findById(family_id: number,isFull: Boolean =true): Promise<IFamilyAccount | null> {
        console.log(await family_repository.getFamilyWithOwnersByIdDB(family_id))
        console.log(isFull)

        return null;

    }
    
    
    async create(family_DTO: ICreateFamilyAccountDTO,isFull: Boolean =true)
    :Promise<IFamilyAccount | null> {
        console.log(family_DTO);
        console.log(isFull);
        const response = await new Promise(() =>"remove this line");
        console.log(response);

        return  null;
    
    }

    async addIndividuals(individuals_ids:number[],family_id:number){

        await family_repository.addIndividualsToFamilyDB(individuals_ids,family_id);
        console.log("success")
    }

}


const family_service = new FamilyService();

export default family_service;
