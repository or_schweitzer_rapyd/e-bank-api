/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import account_service from './account.service.js';

import transaction_repository from '../repositories/transaction.repository.js';
import { ITransactionDB } from '../models/transaction.db.model.js';
import fetch from 'node-fetch';
import { ITransactionDTO } from '../models/account.dto.model.js';
import { IAccount } from '../models/account.model.js';
import business_service from './business.service.js';

class TransferService {
  async getRate(base_currency: string, target_currency: string): Promise<number> {
    console.log(base_currency);
    console.log(target_currency);
    const base_url = `http://api.exchangeratesapi.io/latest`;
    const url = `${base_url}?base=${base_currency}&symbols=${target_currency}&access_key=45b9cb933be71c52613afc30f35b3bd6`;
    let response = await fetch(url);
    let json = (await response.json()) as any;
    console.log({ json });
    if (!('rates' in json)) throw new Error(JSON.stringify(json));
    else if (json.rates[target_currency]) return json.rates[target_currency];
    else {
      throw new Error(`currency: ${target_currency} doesn't exist in results.`); //app logic error
    }
  }

  private async prepareTransaction(
    src_account: IAccount,
    dest_account: IAccount,
    amount: number,
    transaction_type: string,
  ): Promise<[number, number, ITransactionDB]> {
    console.log('start preparing transaction');
    let rate = 1;
    //check if same currency
    console.log('check currency');
    if (src_account.currency !== dest_account.currency) {
      //then get rate
      console.log('get rate');
      rate = await this.getRate(src_account.currency, dest_account.currency);
      console.log(`rate is ${rate}`);
    }

    console.log('preparing transacation db model');
    //prepare transaction db model
    const transaction_db_model: ITransactionDB = {
      src_id: src_account.account_id as number,
      dest_id: dest_account.account_id as number,
      transaction_type,
      amount,
    };

    return [
      src_account.balance - amount,
      dest_account.balance + amount * rate,
      transaction_db_model,
    ];
  }

  async createTransactionB2B(transaction_dto: ITransactionDTO) {
    const [src_account, dest_account] = await account_service.getTwoAccounts([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    //*********************** */
    //TO DO: validation here
    //*********************** */

    const result = await this.prepareTransaction(
      src_account,
      dest_account,
      transaction_dto.amount,
      'B2B',
    );
    const src_balance_after = result[0];
    const dest_balance_after = result[1];
    const transaction_db_model = result[2];

    //execute transaction
    await transaction_repository.transaction(
      transaction_db_model,
      src_balance_after,
      dest_balance_after,
    );

    //return accounts
    console.log('returning results of transfer');
    const source = await business_service.findById(src_account.account_id as number);
    const dest = await business_service.findById(dest_account.account_id as number);

    return { source, dest };
  }

  async createTransactionB2I(transaction_dto: ITransactionDTO) {
    const [src_account, dest_account] = await account_service.getTwoAccounts([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    //*********************** */
    //TO DO: validation here
    //*********************** */

    const result = await this.prepareTransaction(
      src_account,
      dest_account,
      transaction_dto.amount,
      'B2I',
    );
    const src_balance_after = result[0];
    const dest_balance_after = result[1];
    const transaction_db_model = result[2];

    //execute transaction
    await transaction_repository.transaction(
      transaction_db_model,
      src_balance_after,
      dest_balance_after,
    );

    //return accounts
    console.log('returning results of transfer');
    const source = await business_service.findById(src_account.account_id as number);
    const dest = await business_service.findById(dest_account.account_id as number);

    return { source, dest };
  }

  async createTransactionF2B(transaction_dto: ITransactionDTO) {
    const [src_account, dest_account] = await account_service.getTwoAccounts([
      transaction_dto.src,
      transaction_dto.dest,
    ]);
    //*********************** */
    //TO DO: validation here
    //*********************** */

    const result = await this.prepareTransaction(
      src_account,
      dest_account,
      transaction_dto.amount,
      'F2B',
    );
    const src_balance_after = result[0];
    const dest_balance_after = result[1];
    const transaction_db_model = result[2];

    //execute transaction
    await transaction_repository.transaction(
      transaction_db_model,
      src_balance_after,
      dest_balance_after,
    );

    //return accounts
    console.log('returning results of transfer');
    const source = await business_service.findById(src_account.account_id as number);
    const dest = await business_service.findById(dest_account.account_id as number);

    return { source, dest };
  }
}

const transfer_service = new TransferService();
export default transfer_service;
